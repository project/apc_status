api = 2
core = 7.x

libraries[APC][download][type] = "file"
libraries[APC][download][url] = "https://git.php.net/?p=pecl/caching/apc.git;a=blob_plain;f=apc.php;hb=master"
libraries[APC][download][filename] = "apc.php.inc"
